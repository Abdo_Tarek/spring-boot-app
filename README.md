# spring boot app 



## Getting started

### Run without [ Docker ]
1- please ensure that install openjdk:11

2- please ensure that install Maven

3- please ensure that install Docker

4- Create a skeleton application using https://start.spring.io.

5- Now start the application by running : 

``./mvnw spring-boot:run``

### Run with [ Docker ]
1- Ceate a new jar file using maven builder : 

``./mvnw clean package``

2- ``docker build -t [name:tag] .``

3- ``docker run -d -p 8080:8080 [name:tag] ``

4 - ``docker ps``
